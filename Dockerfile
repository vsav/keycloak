FROM jboss/keycloak:16.0.0
ENV KEYCLOAK_METRICS_SPI_VERSION 2.5.3
ENV DEPLOYMENTS_DIR /opt/jboss/keycloak/standalone/deployments
ADD --chown=jboss:root https://github.com/aerogear/keycloak-metrics-spi/releases/download/$KEYCLOAK_METRICS_SPI_VERSION/keycloak-metrics-spi-$KEYCLOAK_METRICS_SPI_VERSION.jar $DEPLOYMENTS_DIR
RUN cd /opt/jboss/keycloak/standalone/deployments && sh -c 'touch keycloak-metrics-spi-2.5.3.jar.dodeploy'
